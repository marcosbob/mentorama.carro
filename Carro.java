public class Carro {

        public static final String VERMELHO = "Vermelha";
        public static final boolean TETOSOLAR = false;
        private String fabricante;
        private String modelo;
        private String cor;
        private String chassis;
    
        private int ano;
        private int velocidade;
        private boolean motorLigado;
        private boolean acessorio;
    
        private Integer quantidadePneus;
        private Integer quantidadeCalotas;
        private Integer quantidadeParafusosPneus;
        private Integer quantidadePortas;
    
        public Carro(Integer quantidadePneus){
            setQuantidadePneus(quantidadePneus);
        }
    
        public static String getVERMELHO() {
            return VERMELHO;
        }

        public static boolean getTETOSOLAR() {
            return TETOSOLAR;
        }
    
        public String getFabricante() {
            return fabricante;
        }
    
        public String getModelo() {
            return modelo;
        }
    
        public String getCor() {
            return cor;
        }
    
        public String getChassis() {
            return chassis;
        }
    
        public int getAno() {
            return ano;
        }
    
        public int getVelocidade() {
            return velocidade;
        }
    
        public boolean isMotorLigado() {
            return motorLigado;
        }

        public boolean isAcessorio() {
            return acessorio;
        }
    
        public Integer getQuantidadePneus() {
            return quantidadePneus + 1;
        }
    
        public Integer getQuantidadeCalotas() {
            return quantidadeCalotas;
        }
    
        public Integer getQuantidadeParafusosPneus() {
            return quantidadeParafusosPneus;
        }
    
        public Integer getQuantidadePortas() {
            return quantidadePortas;
        }
    
        public void setFabricante(String fabricante) {
            this.fabricante = fabricante;
        }
    
        public void setModelo(String modelo) {
            this.modelo = modelo;
        }
    
        public void setCor(String cor) {
            this.cor = cor;
        }
    
        public void setChassis(String chassis) {
            this.chassis = chassis;
        }
    
        public void setAno(int ano) {
            this.ano = ano;
        }
    
    
        public void setVelocidade(int velocidade) {
            this.velocidade = velocidade;
        }
    
        public void setMotorLigado(boolean motorLigado) {
            this.motorLigado = motorLigado;
        }

        public void setAcessorio(boolean acessorio) {
            this.acessorio = acessorio;
        }
    
        public void setQuantidadePneus(Integer quantidadePneus) {
            quantidadeCalotas = quantidadePneus;
            quantidadeParafusosPneus = quantidadePneus * 4;
            quantidadePortas = quantidadePneus;
            this.quantidadePneus = quantidadePneus;
        }

        public void setquantidadePortas(Integer quantidadePortas) {
            this.quantidadePortas = quantidadePortas;
        }
    
        public void exibe() {
            System.out.println("Fabricante:" + getFabricante());
            System.out.println("Modelo:" + getModelo());
            System.out.println("Quantidade de Portas: " + getQuantidadePortas()+ " portas");
            System.out.println("Chassis: " + getChassis());
            System.out.println("Ano:" + getAno());
            System.out.println("Cor:" + getCor());
            System.out.println("Quantidade de Pneus:" + getQuantidadePneus());
            System.out.println("Quantidade de Calotas: " + getQuantidadeCalotas());
            System.out.println("Quantidade de Parafusos: " + getQuantidadeParafusosPneus());
            System.out.println("Acessório Teto Solar: " + getTETOSOLAR());
            
    
    
        }
    }
